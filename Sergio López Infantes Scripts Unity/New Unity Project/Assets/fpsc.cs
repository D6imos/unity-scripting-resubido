﻿using UnityEngine;

public class CapsuleScaling: MonoBehaviour
{
    [serializeField]
    private Vector3 axes;
    public float scaleUnits; 

    void Update()
    {

        axes = CapsuleMovement.ClampVector3(axes);

        transform.localScale += axes * (scaleUnits * Time.deltaTime);

    }
    

}




